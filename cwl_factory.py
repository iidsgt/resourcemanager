from jinja2 import Template
from docker_factory import Container
from cwl_utils.parser_v1_0 import load_document


class cwl_tool_factory (object):
    def __init__(self, cwl):
        self.cwl = load_document(cwl)
        self.inputs = []
        self.arguments = []
        self.defined_inputs = []
        self.basecommand = self.cwl.baseCommand
        

        for i in self.cwl.inputs:
            i_dict = {'id':i.id,
                    'position': None,
                    'prefix':None,
                    'py_type':str,
                    'cw_type':i.type,
                    'value':None,
                    'vf':None
            }
            if i.inputBinding.position:
                i_dict['position'] = i.inputBinding.position
            if i.inputBinding.prefix:
                i_dict['prefix'] = i.inputBinding.prefix
            if i.inputBinding.valueFrom:
                i_dict['vf'] = i.inputBinding.valueFrom
            if i.type == 'boolean':
                i_dict['py_type'] = bool
            self.inputs.append(i_dict)

        for j in self.cwl.arguments:
            a_dict = {'position': None,
                    'prefix': None,
                    'py_type':str,
                    'value':None,
                    'vf': None
            }
            if isinstance(j, str):
                if '$' in j:
                    a_dict['vf'] = j
                else:
                    a_dict['value'] = j
            else:
                if j.prefix:
                    a_dict['prefix'] = j.prefix
                if j.position:
                    a_dict['position'] = j.position
                if j.valueFrom:
                    i_dict['vf'] = j.valueFrom
            self.arguments.append(a_dict)



    @property
    def positional_inputs(self):
        return sorted(filter(lambda x : x['position'] is not None, self.inputs), key=lambda x: x['position'])
    @property
    def nonpositional_inputs(self):
        return list(filter(lambda x: x['position'] is None, self.inputs))

    @property
    def positional_arguments(self):
        return sorted(filter(lambda x : x['position'] is not None, self.arguments), key=lambda x: x['position'])
    
    @property
    def nonpositional_arguments(self):
        return sorted(filter(lambda x : x['position'] is not None, self.arguments), key=lambda x: x['position'])

    @property
    def commandline(self):
        arguments = []
        pos_args = map(lambda x: x['id'] in self.defined_inputs, self.positional_inputs)
        nonpos_args = map(lambda x: x['id'] in self.defined_inputs, self.nonpositional_inputs)


        for j in self.positional_arguments:
            arguments += self.gen_argument_array(j)
        for j in self.nonpositional_arguments:
            arguments += self.gen_argument_array(j)
        for j in pos_args:
            arguments += self.gen_argument_array(j)
        for j in nonpos_args:
            arguments += self.gen_argument_array(j)
            
        return [self.basecommand] + arguments


    
    def __gen_argument_array(self, args):
        x = []
        if args['prefix']:
            x.append(args['prefix'])
        if args['value']:
            x.append(args['value'])
        self.arguments += x



    def set_value(self, idx, value):
        for i, v in enumerate(self.inputs):
            if v['id'] == idx:
                self.inputs[i]['value'] = value
                break

    def add_defined_input(self, idx):
        self.defined_inputs.append(idx)

# arguments : string, Expression, commandline binding, 
#   - Need to check if it is string or a class

# need to add support for piping stdout /stdin

#Need to add javascript checking on all values and anywhere it can be included. 

# Call style will be
# c = cwl_tool_factory('path/to/tool')
# c.add_defined_input(input_id)
# c.set_value(input_id, value)
# c.process_javascript()
# c.commandline

def build_command():
    cwl = load_document('/Users/djevanclark/Documents/git/cromwell/cwl/src/test/resources/1st-tool.cwl')
    inputs = []
    arguments = []
    basecommand = cwl.baseCommand
    if cwl.arguments:
        for arg in cwl.arguments:
            inputs.append(())
        
build_command()
