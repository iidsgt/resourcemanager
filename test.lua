local lfs = require"lfs"
local os = require"os"
local io = require"io"
local shell = require "shell-games"

local BASECOMMAND = { {% for c in basecommand %}'{{c}}',{% endfor %} }
local WORKDIR = '{{workdir}}'
local FILEGROUPS = { {% for g in groups %}{ name = '{{g["name"]}}', files = { {%for f in g["files"]%}'{{f}}',{% endfor %}{{'}'}}, md5 = { {%for m in g["md5"]%}'{{m}}',{% endfor %}} },{% endfor %} }
local RUNTIME = { {% for r in runtime %}'{{r}}',{% endfor %} }

function execute(command)
    local config = {
        capture = true,
        stderr = "./cmd.stderr",
        stdout = "./cmd.stdout",
        chdir = "./execution"
    }
    local result, err = shell.run(command, config)
    if err then
        file = io.open('./rc', 'w')
        file.write("1")
        file.close()
    else
        file = io.open('./rc', 'w')
        file.write(result["status"])
        file.close()
    end
end

function execute_container(comand, runtime)
    local config = {
        capture = true,
        stderr = "./cmd.stderr",
        stdout = "./cmd.stdout",
        chdir = "./execution"
    }
    local cmd_extended = runtime + command
    local result, err = shell.run(cmd_extended, config)
    if err then
        file = io.open('./rc', 'w')
        file.write("1")
        file.close()
    else
        file = io.open('./rc', 'w')
        file.write(result["status"])
        file.close()
    end
end


function generate_directory()
    lfs.mkdir("execution")
    lfs.mkdir("inputs")
end

function localize_files(fg)
    for group in fg do
        lfs.mkdir(group.name)
        for file in group.files do
            local result, err = shell.run({ "cp", "-r", file, './inputs/' .. group.name .. '/'})
            if err then
                file = io.open('./fc', 'w')
                file.write("1")
                file.close()
                os.exit(1)
            if result['status'] != 0 then
                file = io.open('./fc', 'w') --This reports to the filesystem if the file copying has failed or succeeded.
                file.write("File localization failed: " .. file)
                file.close()
                os.exit(1)
            else
                return
            end
        end
    end
end




lfs.chdir(WORKDIR)