class Container(object):
    def __init__(self, container, runtime):
        self.volumes = []
        self.boolean_arguments = []
        self.container = container
        self.runtime = runtime
        self.run_command = 'run'
    def add_volume(self, mount_from, mount_to):
        #check if is path
        self.volumes.append((mount_from, mount_to))
    def add_flags(self, flag):
        self.boolean_arguments.append(flag)
    @property
    def commandline(self):
        command = []
        command += [self.runtime]
        command += [self.run_command]
        command += self.boolean_arguments
        for v in self.volumes:
            l = ['-v', '%s:%s' % v ]
            command += l
        command += [self.container]
        return command
