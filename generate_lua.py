from jinja2 import Template
from .docker_factory import Container
from cwl_utils.parser_v1_0 import load_document




with open('./tes.jinja') as f:
    template = Template(f.read())
    output = template.render(groups=[{"name":"test", "files":["1", "2"], "md5":["1","3"]}])
    print(output)